var hubServer = 'http://api.hub.app/';
var hubEndpoint = 'v1/wishes/';

var supportedHosts = [
    'www.imdb.com',
    'imdb.com',
];

var containerStyle = 'font-family: "Segoe UI", "Helvetica Neue", Helvetica, Arial, sans-serif; font-weight: bold; position: fixed; z-index: 9999999999; width: 100%; padding-top: 20px; padding-bottom: 20px; text-align: center; border-bottom: 10px solid #262930; background-color: #2a2e35; color: #fff; cursor: pointer; cursor: hand;';

var body = document.querySelector('body');
var container = document.getElementById('hub');
if (!container) {
    var container = document.createElement('div');
    container.id = 'hub';
    container.style = containerStyle;
    container.addEventListener('click', function () {
        while (container.firstChild) {
            container.removeChild(container.firstChild);
        }

        body.removeChild(container);
    }, false);

    body.insertBefore(container, body.firstChild);
}
else {
    container.style = containerStyle;
}

var pathArray = window.location.pathname.split('/');

if (supportedHosts.indexOf(window.location.host) > -1) {
    if (pathArray[1] == 'title' && 2 in pathArray) {
        container.innerHTML = 'Sending request to ' + hubServer + ' ...';

        var request = new XMLHttpRequest();
        request.open('POST', hubServer + hubEndpoint, true);
        request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        request.onreadystatechange = function () {
            if (request.readyState == XMLHttpRequest.DONE) {
                if (request.status == 201) {
                    var successObject = eval('(' + request.responseText + ')');
                    container.style = containerStyle + 'background-color: #32CD32;';

                    container.innerHTML = successObject.data.message + ' "' + successObject.data.created_record.title + '"';
                } else {
                    var errorObject = eval('(' + request.responseText + ')');
                    container.style = containerStyle + 'background-color: #F08080;';

                    if (errorObject.error.message.imdb_id) {
                        container.innerHTML = errorObject.error.message.imdb_id[0];
                    }
                    else {
                        container.innerHTML = errorObject.error.message;
                    }
                }
            }
            return;
        };
        request.send('imdb_id=' + pathArray[2]);
    }
    else {
        container.innerHTML = 'Did not find a suitable ID to add from the URL: ' + window.location.href;
    }
}
else {
    container.innerHTML = window.location.host + ' is not supported. Please only use this bookmark on ' + supportedHosts.join(', ');
}