/* ============================================================
 * File: main.js
 * Main Controller to set global scope variables. 
 * ============================================================ */

angular.module('app')
    .filter('bytes', function () {
        return function (bytes, precision) {
            if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
            if (typeof precision === 'undefined') precision = 1;
            var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
                number = Math.floor(Math.log(bytes) / Math.log(1024));
            return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + units[number];
        }
    })
    .filter('inArray', function () {
        return function (array, value) {
            for (var key in array) {
                if (array[key].torrent_id == value) {
                    return true;
                }
            }

            return false;
        };
    })
    .filter('stripAfterSlash', function () {
        return function (item) {
            return item.replace(/\/.*/g, '');
        }
    })
    .filter("asDate", function () {
        return function (input) {
            return new Date(input);
        }
    })
    .controller('AppCtrl', ['$scope', '$rootScope', '$state', function ($scope, $rootScope, $state) {

        // App globals
        $scope.app = {
            name: 'Hub4',
            description: '',
            layout: {
                menuPin: true,
                menuBehind: false,
                theme: 'pages/css/themes/retro.css'
            },
            author: 'blackmage'
        }

        // Checks if the given state is the current state
        $scope.is = function (name) {
            return $state.is(name);
        }

        // Checks if the given state/child states are present
        $scope.includes = function (name) {
            return $state.includes(name);
        }

        // Broadcasts a message to pgSearch directive to toggle search overlay
        $scope.showSearchOverlay = function () {
            $scope.$broadcast('toggleSearchOverlay', {
                show: true
            })
        }

    }]);

angular.module('app')
    /*
     Use this directive together with ng-include to include a
     template file by replacing the placeholder element
     */

    .directive('includeReplace', function () {
        return {
            require: 'ngInclude',
            restrict: 'A',
            link: function (scope, el, attrs) {
                el.replaceWith(el.children());
            }
        };
    })