'use strict';

/* Controllers */

angular.module('app', ['ui.bootstrap']);
angular.module('app')
    .controller('SeriesDetailsController', ['$scope', '$http', '$stateParams', '$uibModal', function ($scope, $http, $stateParams, $uibModal) {
        $scope.seriesLoading = false;
        $scope.episodesLoading = false;

        $scope.init = function () {
            $scope.seriesLoading = true;
            $scope.episodesLoading = true;

            $http.get('http://api.hub.app/v1/series/' + $stateParams.seriesId).then(
                function(success) {
                    $scope.series = success.data.data;
                    $scope.seriesLoading = false;
                },
                function(error) {

                }
            );

            $http.get('http://api.hub.app/v1/series/' + $stateParams.seriesId + '/episodes').then(
                function (success) {
                    $scope.episodes = success.data.data;
                    $scope.episodesLoading = false;
                },
                function (error) {

                }
            );
        }

        $scope.showTorrentsModal = function (episode) {
            $scope.episode = episode;

            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'tpl/modals/episode_torrents.html',
                controller: 'TorrentsModalController',
                resolve: {
                    episode: function () {
                        return $scope.episode;
                    },
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                'assets/plugins/angular-ui-bootstrap/angular-ui-bootstrap.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function () {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/dashboard.js'
                                ]);
                            });
                    }]
                }
            });

            modalInstance.result.then(function () {
            }, function () {
            });
        }

        $scope.init();

        $scope.refreshSeries = function(series) {
            alert('Not implemented yet');
        }

        $scope.deleteSeries = function(series) {
            alert('Not implemented yet');
        }

        $scope.searchEpisode = function(episode) {
            alert('Not implemented yet');
        }

        $scope.deleteEpisode = function(episode) {
            alert('Not implemented yet');
        }
    }]);