'use strict';

/* Controllers */

angular.module('app', ['ui.bootstrap']);
angular.module('app')
    .controller('FeedsController', ['$scope', '$http', '$uibModal', '$log', function ($scope, $http, $uibModal, $log) {
        $scope.loading = false;

        $scope.feeds = [];

        $scope.init = function () {
            $scope.loading = true;
            $http.get('http://api.hub.app/v1/feeds/').then(
                function (success) {
                    $scope.feeds = success.data.data;
                    $scope.loading = false;
                },
                function (error) {

                }
            );
        }

        $scope.refreshAll = function() {
            $http.get('http://api.hub.app/v1/feeds/refresh').then(
                function (success) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: success.data.data.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'success'
                    }).show();
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        }

        $scope.openAddFeedModal = function() {
            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'tpl/modals/add_feed.html',
                controller: 'FeedModalController',
                resolve: {
                    feed: function () {
                        return null
                    },
                    feeds: function () {
                        return $scope.feeds;
                    },
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                'assets/plugins/angular-ui-bootstrap/angular-ui-bootstrap.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function () {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/folders.js'
                                ]);
                            });
                    }]
                }
            });

            modalInstance.result.then(function () {
            }, function () {
            });
        }

        $scope.openDeleteFeedModal = function ($event, feed) {
            $scope.feed = feed;

            $log.info(feed);

            if ($event.shiftKey) {
                $log.info('Shift key was down');
            }
            else {
                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'tpl/modals/delete_feed.html',
                    controller: 'FeedModalController',
                    resolve: {
                        feed: function () {
                            return $scope.feed;
                        },
                        feeds: function () {
                            return $scope.feeds;
                        },
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/plugins/angular-ui-bootstrap/angular-ui-bootstrap.js'
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/feeds.js'
                                    ]);
                                });
                        }]
                    }
                });

                modalInstance.result.then(function () {
                }, function () {
                });
            }
        }

        $scope.init();
    }]);

angular.module('app')
    .controller('FeedModalController', function ($scope, $http, $uibModalInstance, $log, feed, feeds) {
        $scope.feed = feed;
        $scope.feeds = feeds;

        $scope.formData = {
            keepWindowOpen: false,
        };

        $scope.add = function () {
            $scope.message = '';
            $scope.errorTitle = '';
            $scope.errorUri = '';

            $http.post('http://api.hub.app/v1/feeds/', {
                'title': $scope.formData.title,
                'uri': $scope.formData.uri,
            }).then(
                function (success) {
                    $scope.feeds.push(success.data.data.created_record);

                    if ($scope.formData.keepWindowOpen == true) {
                        $scope.formData.title = '';
                        $scope.formData.uri = '';
                        $scope.message = success.data.data.message;
                    }
                    else {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data.data.message,
                            position: 'top',
                            timeout: 3000,
                            type: 'success'
                        }).show();

                        $uibModalInstance.close($scope.feeds);
                    }
                },
                function (error) {
                    if(error.data.error.message.title) {
                        $scope.errorTitle = error.data.error.message.title[0];
                    }
                    if(error.data.error.message.uri) {
                        $scope.errorUri = error.data.error.message.uri[0];
                    }
                }
            );
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.delete = function () {
            $http.delete('http://api.hub.app/v1/feeds/' + feed.id).then(
                function (success) {
                    for (var i = 0; i < $scope.feeds.length; i++) {
                        if ($scope.feeds[i].id == $scope.feed.id) {
                            $scope.feeds.splice(i, 1);

                            $('body').pgNotification({
                                style: 'bar',
                                message: success.data.data.message,
                                position: 'top',
                                timeout: 3000,
                                type: 'success'
                            }).show();
                            break;
                        }
                    }

                    $uibModalInstance.close($scope.feeds);
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        };
    });
