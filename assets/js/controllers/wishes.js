'use strict';

/* Controllers */

angular.module('app', ['ui.bootstrap']);
angular.module('app')
    .controller('WishesController', ['$scope', '$http', '$log', '$uibModal', function ($scope, $http, $log, $uibModal) {
        $scope.loading = false;

        $scope.wishes = [];

        $scope.init = function () {
            $scope.loading = true;
            $http.get('http://api.hub.app/v1/wishes/').then(
                function (success) {
                    $scope.wishes = success.data.data;
                    $scope.loading = false;
                },
                function (error) {

                }
            );
        }

        $scope.init();

        $scope.refreshWishes = function () {
            $http.get('http://api.hub.app/v1/wishes/refresh').then(
                function (success) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: success.data.data.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'success'
                    }).show();
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        }

        $scope.refreshWish = function (wish) {
            $http.get('http://api.hub.app/v1/wishes/' + wish.id + '/refresh').then(
                function (success) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: success.data.data.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'success'
                    }).show();
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        }

        $scope.openDeleteWishModal = function ($event, wish) {
            $scope.wish = wish;

            if ($event.shiftKey) {
                $log.info('Shift key was down');
            }
            else {
                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'tpl/modals/delete_wish.html',
                    controller: 'WishModalController',
                    resolve: {
                        wish: function () {
                            return $scope.wish;
                        },
                        listOfWishes: function () {
                            return $scope.wishes;
                        },
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/plugins/angular-ui-bootstrap/angular-ui-bootstrap.js'
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/wishes.js'
                                    ]);
                                });
                        }]
                    }
                });

                modalInstance.result.then(function () {
                }, function () {
                });
            }
        }
    }]);

angular.module('app')
    .controller('WishModalController', function ($scope, $http, $uibModalInstance, $log, wish, listOfWishes) {
        $scope.wish = wish;
        $scope.listOfWishes = listOfWishes;

        $scope.delete = function () {
            $http.delete('http://api.hub.app/v1/wishes/' + wish.id).then(
                function (success) {
                    for (var i = 0; i < $scope.listOfWishes.length; i++) {
                        if ($scope.listOfWishes[i].id == $scope.wish.id) {
                            $scope.listOfWishes.splice(i, 1);

                            $('body').pgNotification({
                                style: 'bar',
                                message: success.data.data.message + ': ' + success.data.data.deleted_record.title,
                                position: 'top',
                                timeout: 3000,
                                type: 'success'
                            }).show();
                            break;
                        }
                    }

                    $uibModalInstance.close();
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });