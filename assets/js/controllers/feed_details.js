'use strict';

/* Controllers */

angular.module('app')
    .controller('FeedDetailsController', ['$scope', '$http', '$stateParams', '$log', function ($scope, $http, $stateParams, $log) {
        $scope.feedLoading = false;

        if ($stateParams.page) {
            $scope.feedUri = 'http://api.hub.app/v1/feeds/' + $stateParams.feedId + '?page=' + $stateParams.page;
        }
        else {
            $scope.feedUri = 'http://api.hub.app/v1/feeds/' + $stateParams.feedId;
        }

        $scope.init = function () {
            $scope.feedLoading = true;
            $http.get($scope.feedUri).then(
                function (success) {
                    $scope.feed = success.data.data.feed;
                    $scope.torrents = success.data.data.torrents;

                    $scope.feedLoading = false;
                },
                function (error) {
                    $log.error(error);
                }
            );
        }

        $scope.downloadTorrent = function (torrent) {
            $log.info('downloadTorrent');
            $log.info(torrent);
        }

        $scope.init();
    }]);