angular.module('app')
    .controller('TorrentsModalController', function ($scope, $http, $uibModalInstance, $log, episode) {
        $scope.episode = episode;

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.downloadTorrent = function (torrent) {
            $http.get('http://api.hub.app/v1/torrents/' + torrent.id + '/download')
                .then(
                    function (success) {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data.data.message + ' "' + success.data.data.downloaded_record.title + '"',
                            position: 'top',
                            timeout: 3000,
                            type: 'success'
                        }).show();

                        $uibModalInstance.close();
                    },
                    function (error) {
                        $('body').pgNotification({
                            style: 'bar',
                            message: error.data.error.message,
                            position: 'top',
                            timeout: 3000,
                            type: 'error'
                        }).show();
                    }
                );
        }
    });