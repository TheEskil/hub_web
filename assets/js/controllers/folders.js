'use strict';

/* Controllers */

angular.module('app', ['ui.bootstrap']);
angular.module('app')
    .controller('FoldersController', ['$scope', '$http', '$uibModal', '$log', function ($scope, $http, $uibModal, $log) {
        $scope.loading = false;

        $scope.folders = [];

        $scope.init = function () {
            $scope.loading = true;
            $http.get('http://api.hub.app/v1/folders/').then(
                function (success) {
                    $scope.folders = success.data.data;
                    $scope.loading = false;
                },
                function (error) {

                }
            );
        }

        $scope.openAddFolderModal = function() {
            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'tpl/modals/add_folder.html',
                controller: 'FolderModalController',
                resolve: {
                    folder: function() {
                        return null
                    },
                    folders: function () {
                        return $scope.folders;
                    },
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                'assets/plugins/angular-ui-bootstrap/angular-ui-bootstrap.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function () {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/folders.js'
                                ]);
                            });
                    }]
                }
            });

            modalInstance.result.then(function () {
            }, function () {
            });
        }

        $scope.openDeleteFolderModal = function ($event, folder) {
            $scope.folder = folder;

            $log.info(folder);

            if ($event.shiftKey) {
                $log.info('Shift key was down');
            }
            else {
                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'tpl/modals/delete_folder.html',
                    controller: 'FolderModalController',
                    resolve: {
                        folder: function () {
                            return $scope.folder;
                        },
                        folders: function () {
                            return $scope.folders;
                        },
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/plugins/angular-ui-bootstrap/angular-ui-bootstrap.js'
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/series.js'
                                    ]);
                                });
                        }]
                    }
                });

                modalInstance.result.then(function () {
                }, function () {
                });
            }
        }

        $scope.init();
    }]);

angular.module('app')
    .controller('FolderModalController', function ($scope, $http, $uibModalInstance, $log, folder, folders) {
        $scope.folder = folder;
        $scope.folders = folders;

        $scope.formData = {
            keepWindowOpen: false,
        };

        $scope.add = function () {
            $scope.message = '';
            $scope.errorName = '';

            $http.post('http://api.hub.app/v1/folders/', {
                'name': $scope.formData.name,
            }).then(
                function (success) {
                    $scope.folders.push(success.data.data.created_record);

                    if ($scope.formData.keepWindowOpen == true) {
                        $scope.formData.name = '';
                        $scope.message = success.data.data.message;
                    }
                    else {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data.data.message,
                            position: 'top',
                            timeout: 3000,
                            type: 'success'
                        }).show();

                        $uibModalInstance.close($scope.folders);
                    }
                },
                function (error) {
                    $scope.errorName = error.data.error.message.name[0];
                }
            );
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.delete = function () {
            $http.delete('http://api.hub.app/v1/folders/' + folder.id).then(
                function (success) {
                    for (var i = 0; i < $scope.folders.length; i++) {
                        if ($scope.folders[i].id == $scope.folder.id) {
                            $scope.folders.splice(i, 1);

                            $('body').pgNotification({
                                style: 'bar',
                                message: success.data.data.message + ': ' + success.data.data.deleted_record.name,
                                position: 'top',
                                timeout: 3000,
                                type: 'success'
                            }).show();
                            break;
                        }
                    }

                    $uibModalInstance.close($scope.folders);
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        };
    });
