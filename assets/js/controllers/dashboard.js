'use strict';

/* Controllers */

angular.module('app', ['ui.bootstrap']);
angular.module('app')
    .controller('DashboardController', ['$scope', '$http', '$log', '$uibModal', function ($scope, $http, $log, $uibModal) {
        $scope.recentEpisodesLoading = false;
        $scope.upcomingEpisodesLoading = false;

        $scope.recentEpisodes = [];
        $scope.upcomingEpisodes = [];

        $scope.init = function () {
            $scope.recentEpisodesLoading = true;
            $http.get('http://api.hub.app/v1/episodes/recent/3').then(
                function (success) {
                    $scope.recentEpisodes = success.data.data;
                    $scope.recentEpisodesLoading = false;
                },
                function (error) {

                }
            );

            $scope.upcomingEpisodesLoading = true;
            $http.get('http://api.hub.app/v1/episodes/upcoming').then(
                function (success) {
                    $scope.upcomingEpisodes = success.data.data;
                    $scope.upcomingEpisodesLoading = false;
                },
                function (error) {

                }
            );
        }

        $scope.showTorrentsModal = function (episode) {
            $scope.episode = episode;

            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'tpl/modals/episode_torrents.html',
                controller: 'TorrentsModalController',
                resolve: {
                    episode: function () {
                        return $scope.episode;
                    },
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                'assets/plugins/angular-ui-bootstrap/angular-ui-bootstrap.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function () {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/dashboard.js'
                                ]);
                            });
                    }]
                }
            });

            modalInstance.result.then(function () {
            }, function () {
            });
        }

        $scope.init();
    }]);