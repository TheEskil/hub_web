'use strict';

/* Controllers */

angular.module('app')
    .controller('TransmissionController', ['$scope', '$http', '$log', function ($scope, $http, $log) {
        $scope.loading = false;

        $scope.torrents = [];

        $scope.init = function () {
            $scope.loading = true;
            $http.get('http://api.hub.app/v1/transmission/').then(
                function (success) {
                    if (success.data.error) {
                        $scope.error = success.data.error;
                    }
                    else {
                        $scope.torrents = success.data.data;
                    }

                    $scope.loading = false;
                },
                function (error) {
                    $scope.loading = false;

                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        }

        $scope.startAll = function () {
            $http.get('http://api.hub.app/v1/transmission/start/all').then(
                function (success) {
                    if (success.data.error) {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data.error.message,
                            position: 'top',
                            timeout: 3000,
                            type: 'error'
                        }).show();
                    }
                    else {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data,
                            position: 'top',
                            timeout: 3000,
                            type: 'success'
                        }).show();
                    }
                },
                function (error) {
                    $log.error(error);
                }
            );
        }

        $scope.stopAll = function () {
            $http.get('http://api.hub.app/v1/transmission/stop/all').then(
                function (success) {
                    if (success.data.error) {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data.error.message,
                            position: 'top',
                            timeout: 3000,
                            type: 'error'
                        }).show();
                    }
                    else {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data,
                            position: 'top',
                            timeout: 3000,
                            type: 'success'
                        }).show();
                    }
                },
                function (error) {
                    $log.error(error);
                }
            );
        }

        $scope.removeAllFinished = function () {
            $http.get('http://api.hub.app/v1/transmission/remove/finished').then(
                function (success) {
                    if (success.data.error) {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data.error.message,
                            position: 'top',
                            timeout: 3000,
                            type: 'error'
                        }).show();
                    }
                    else {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data,
                            position: 'top',
                            timeout: 3000,
                            type: 'success'
                        }).show();
                    }
                },
                function (error) {
                    $log.error(error);
                }
            );
        }

        $scope.stop = function (torrent) {
            $http.get('http://api.hub.app/v1/transmission/stop/' + torrent.hash).then(
                function (success) {
                    if (success.data.error) {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data.error.message,
                            position: 'top',
                            timeout: 3000,
                            type: 'error'
                        }).show();
                    }
                    else {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data,
                            position: 'top',
                            timeout: 3000,
                            type: 'success'
                        }).show();
                    }
                },
                function (error) {
                    $log.error(error);
                }
            );
        }

        $scope.start = function (torrent) {
            $http.get('http://api.hub.app/v1/transmission/start/' + torrent.hash).then(
                function (success) {
                    if (success.data.error) {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data.error.message,
                            position: 'top',
                            timeout: 3000,
                            type: 'error'
                        }).show();
                    }
                    else {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data,
                            position: 'top',
                            timeout: 3000,
                            type: 'success'
                        }).show();
                    }
                },
                function (error) {
                    $log.error(error);
                }
            );
        }

        $scope.remove = function (torrent, removeLocalData) {
            $http.get('http://api.hub.app/v1/transmission/remove/' + torrent.hash + '/' + removeLocalData).then(
                function (success) {
                    if (success.data.error) {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data.error.message,
                            position: 'top',
                            timeout: 3000,
                            type: 'error'
                        }).show();
                    }
                    else {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data,
                            position: 'top',
                            timeout: 3000,
                            type: 'success'
                        }).show();
                    }
                },
                function (error) {
                    $log.error(error);
                }
            );
        }

        $scope.init();
    }]);
