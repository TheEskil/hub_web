'use strict';

/* Controllers */

angular.module('app')
    .controller('FilesController', ['$scope', '$http', '$log', '$stateParams', function ($scope, $http, $log, $stateParams) {
        $scope.loading = false;

        $scope.files = [];

        if ($stateParams.page) {
            $scope.filesUri = 'http://api.hub.app/v1/files?page=' + $stateParams.page;
        }
        else {
            $scope.filesUri = 'http://api.hub.app/v1/files';
        }

        $scope.init = function () {
            $scope.loading = true;
            $http.get($scope.filesUri).then(
                function (success) {
                    $scope.files = success.data;
                    $scope.loading = false;
                },
                function (error) {
                    $scope.loading = false;
                }
            );
        }

        $scope.init();
    }]);