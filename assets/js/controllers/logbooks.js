'use strict';

/* Controllers */

angular.module('app')
    .controller('LogbooksController', ['$scope', '$http', '$log', function ($scope, $http, $log) {
        $scope.loading = false;

        $scope.logbooks = [];

        $scope.init = function () {
            $scope.loading = true;
            $http.get('http://api.hub.app/v1/logbooks/').then(
                function (success) {
                    $scope.logbooks = success.data.data;
                    $scope.loading = false;
                },
                function (error) {

                }
            );
        }

        $scope.init();
    }]);
