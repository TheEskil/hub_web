'use strict';

/* Controllers */

angular.module('app')
    .controller('FilesDownloadsController', ['$scope', '$http', '$log', function ($scope, $http, $log) {
        $scope.loading = false;

        $scope.files = [];

        $scope.init = function () {
            $scope.loading = true;
            $http.get('http://api.hub.app/v1/files/downloads').then(
                function (success) {
                    $scope.files = success.data.data;
                    $log.info($scope.files);
                    $scope.loading = false;
                },
                function (error) {
                    $scope.loading = false;
                }
            );
        }

        $scope.init();

        $scope.processAllFiles = function() {
            alert('Not implemented yet');
        }

        $scope.processFile = function(file) {
            alert('Not implemented yet');
        }
    }]);
