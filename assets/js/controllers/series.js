'use strict';

/* Controllers */

angular.module('app', ['ui.bootstrap']);
angular.module('app')
    .controller('SeriesController', function ($scope, $http, $uibModal, $log) {
        $scope.loading = false;
        $scope.listOfSeries = [];

        $scope.init = function () {
            $scope.loading = true;
            $http.get('http://api.hub.app/v1/series/').success(function (data, status, headers, config) {
                $scope.listOfSeries = data.data;
                $scope.loading = false;
            });
        }

        $scope.openAddAliasModal = function (series) {
            $scope.series = series;

            var modalInstance = $uibModal.open({
                animation: false,
                templateUrl: 'tpl/modals/add_alias.html',
                controller: 'AliasModalController',
                resolve: {
                    series: function () {
                        return $scope.series;
                    },
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                                'assets/plugins/angular-ui-bootstrap/angular-ui-bootstrap.js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function () {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/series.js'
                                ]);
                            });
                    }]
                }
            });

            modalInstance.result.then(function () {
            }, function () {
            });
        };

        $scope.refresh = function (series) {
            console.log('Refresh series: ' + series);

            $http.get('http://api.hub.app/v1/series/' + series.id + '/refresh').then(
                function(success) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: success.data.data.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'success'
                    }).show();
                },
                function(error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        }

        $scope.refreshAll = function () {
            $http.get('http://api.hub.app/v1/series/refresh').then(
                function (success) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: success.data.data.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'success'
                    }).show();
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        }

        $scope.rebuildEpisodes = function () {
            $http.get('http://api.hub.app/v1/episodes/rebuild').then(
                function (success) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: success.data.data.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'success'
                    }).show();
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        }

        $scope.rebuildFolders = function () {
            $http.get('http://api.hub.app/v1/folders/rebuild').then(
                function (success) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: success.data.data.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'success'
                    }).show();
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        }

        $scope.deleteAlias = function (alias) {
            $log.info('Deleting alias ' + alias.title + ' with id of ' + alias.id);

            outerLoop:
            for (var i = 0; i < $scope.listOfSeries.length; i++) {
                for (var o = 0; o < $scope.listOfSeries[i].aliases.length; o++) {
                    if ($scope.listOfSeries[i].aliases[o].id === alias.id) {
                        $http.delete('http://api.hub.app/v1/aliases/' + alias.id).then(
                            function (success) {
                                $('body').pgNotification({
                                    style: 'bar',
                                    message: success.data.data.message,
                                    position: 'top',
                                    timeout: 3000,
                                    type: 'success'
                                }).show();

                                $scope.listOfSeries[i].aliases.splice(o, 1);
                            },
                            function (error) {
                                $('body').pgNotification({
                                    style: 'bar',
                                    message: error.data.error.message,
                                    position: 'top',
                                    timeout: 3000,
                                    type: 'error'
                                }).show();
                            }
                        );
                        break outerLoop;
                    }
                }
            }
        }

        $scope.openDeleteSeriesModal = function ($event, series) {
            $scope.series = series;

            if($event.shiftKey) {
                $log.info('Shift key was down');
            }
            else {
                var modalInstance = $uibModal.open({
                    animation: false,
                    templateUrl: 'tpl/modals/delete_series.html',
                    controller: 'SeriesModalController',
                    resolve: {
                        series: function () {
                            return $scope.series;
                        },
                        listOfSeries: function () {
                            return $scope.listOfSeries;
                        },
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                    'assets/plugins/angular-ui-bootstrap/angular-ui-bootstrap.js'
                                ], {
                                    insertBefore: '#lazyload_placeholder'
                                })
                                .then(function () {
                                    return $ocLazyLoad.load([
                                        'assets/js/controllers/series.js'
                                    ]);
                                });
                        }]
                    }
                });

                modalInstance.result.then(function () {
                }, function () {
                });
            }
        }

        $scope.init();
    });

angular.module('app')
    .controller('AliasModalController', function ($scope, $http, $uibModalInstance, $log, series) {
        $scope.series = series;

        $scope.formData = {
            keepWindowOpen: false,
        };

        $scope.add = function () {
            $scope.message = '';
            $scope.errorTitle = '';

            $http.post('http://api.hub.app/v1/aliases/', {
                'title': $scope.formData.title,
                'series_id': series.id
            }).then(
                function (success) {
                    $scope.series.aliases.push(success.data.data.created_record);

                    if ($scope.formData.keepWindowOpen == true) {
                        $scope.formData.title = '';
                        $scope.message = success.data.data.message;
                    }
                    else {
                        $('body').pgNotification({
                            style: 'bar',
                            message: success.data.data.message,
                            position: 'top',
                            timeout: 3000,
                            type: 'success'
                        }).show();

                        $uibModalInstance.close($scope.series.aliases);
                    }
                },
                function (error) {
                    $scope.errorTitle = error.data.error.message.title[0];
                }
            );
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });

angular.module('app')
    .controller('SeriesModalController', function ($scope, $http, $uibModalInstance, $log, series, listOfSeries) {
        $scope.series = series;
        $scope.listOfSeries = listOfSeries;

        $scope.delete = function () {
            $http.delete('http://api.hub.app/v1/series/' + series.id).then(
                function (success) {
                    for (var i = 0; i < $scope.listOfSeries.length; i++) {
                        if ($scope.listOfSeries[i].id == $scope.series.id) {
                            $scope.listOfSeries.splice(i, 1);

                            $('body').pgNotification({
                                style: 'bar',
                                message: success.data.data.message + ': ' + success.data.data.deleted_record.title,
                                position: 'top',
                                timeout: 3000,
                                type: 'success'
                            }).show();
                            break;
                        }
                    }

                    $uibModalInstance.close($scope.series.aliases);
                },
                function (error) {
                    $('body').pgNotification({
                        style: 'bar',
                        message: error.data.error.message,
                        position: 'top',
                        timeout: 3000,
                        type: 'error'
                    }).show();
                }
            );
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });